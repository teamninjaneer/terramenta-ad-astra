/**
 * Copyright © 2014, Terramenta. All rights reserved. * This work is subject to the terms of either
 * the GNU General Public License Version 3 ("GPL") or the Common Development and Distribution
 * License("CDDL") (collectively, the "License"). You may not use this work except in compliance
 * with the License.
 *
 * You can obtain a copy of the License at http://opensource.org/licenses/CDDL-1.0
 * http://opensource.org/licenses/GPL-3.0
 *
 */
package com.terramenta.adastra.lunar;

import gov.nasa.worldwind.WorldWind;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.render.BasicWWTexture;
import gov.nasa.worldwind.render.Offset;
import gov.nasa.worldwind.render.PointPlacemark;
import gov.nasa.worldwind.render.PointPlacemarkAttributes;
import gov.nasa.worldwind.render.WWTexture;
import java.net.URL;

/**
 *
 * @author Chris Heidt <heidtmare@gmail.com>
 */
public class SublunarPlacemark extends PointPlacemark {

    public SublunarPlacemark() {
        super(Position.ZERO);

        super.setValue(AVKey.DISPLAY_NAME, "Sublunar Point");
        super.setValue(AVKey.DISPLAY_ICON, "com/terramenta/adastra/lunar/moon.png");//16x16

        PointPlacemarkAttributes attributes = new PointPlacemarkAttributes();
        attributes.setImageOffset(new Offset(0.5, 0.5, AVKey.FRACTION, AVKey.FRACTION));
        URL resource = getClass().getResource("/" + super.getValue(AVKey.DISPLAY_ICON));
        if (resource != null) {
            attributes.setImageAddress(resource.toExternalForm());
        }
        super.setAttributes(attributes);

        super.setAltitudeMode(WorldWind.CLAMP_TO_GROUND);
    }

    /**
     * fixes a wwj bug in 2.0.0 // commenting out till i can determine if still necessary
     *
     * @param address
     * @return
     */
//    @Override
//    protected WWTexture initializeTexture(String address) {
//        URL localUrl = WorldWind.getDataFileStore().requestFile(address);
//        return localUrl == null ? null : new BasicWWTexture(localUrl, true);
//    }
}
