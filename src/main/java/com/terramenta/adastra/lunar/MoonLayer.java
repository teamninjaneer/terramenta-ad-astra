/**
 * Copyright © 2014, Terramenta. All rights reserved. * This work is subject to the terms of either
 * the GNU General Public License Version 3 ("GPL") or the Common Development and Distribution
 * License("CDDL") (collectively, the "License"). You may not use this work except in compliance
 * with the License.
 *
 * You can obtain a copy of the License at http://opensource.org/licenses/CDDL-1.0
 * http://opensource.org/licenses/GPL-3.0
 *
 */
package com.terramenta.adastra.lunar;

import com.terramenta.time.DatetimeChangeListener;
import com.terramenta.time.DatetimeProvider;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.layers.RenderableLayer;
import java.time.Instant;
import java.util.Arrays;
import org.openide.util.Lookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Chris Heidt <heidtmare@gmail.com>
 */
public class MoonLayer extends RenderableLayer {

    private static final Logger LOGGER = LoggerFactory.getLogger(MoonLayer.class);
    private static final DatetimeProvider datetimeProvider = Lookup.getDefault().lookup(DatetimeProvider.class);
    private final DatetimeChangeListener listener = this::update;
    private final MoonPlacemark moonRenderable;
    private final SublunarPlacemark sublunarPlacemark;

    public MoonLayer() {
        super.setName("Moon");
        super.setPickEnabled(false);

        sublunarPlacemark = new SublunarPlacemark();
        sublunarPlacemark.setVisible(true);

        moonRenderable = new MoonPlacemark();
        moonRenderable.setVisible(false);

        //these are the only two renderables allowed in this layer
        super.setRenderables(Arrays.asList(sublunarPlacemark, moonRenderable));

        //toggle datetime listeing based on layer enabled state
        addPropertyChangeListener("Enabled", this::enableDatetimeListener);

        //apply inital listener state
        enableDatetimeListener(super.isEnabled());
    }

    /**
     * Only listen for datetime changes when layer is enabled. PropertyChangeEvents provide all
     * values as Objects.
     *
     * @param enable Object arg from PropertyChangeEvent representing boolean enabled state
     */
    private void enableDatetimeListener(Object enable) {
        if (Boolean.TRUE.equals(enable)) {
            datetimeProvider.addChangeListener(listener);
        } else {
            datetimeProvider.removeChangeListener(listener);
        }
    }

    private void update(Instant previous, Instant current) {
        if (current == null) {
            return;
        }

        //TODO: provide correct moon positioning logic
        Position moonPosition = Position.ZERO;
        LOGGER.debug("The Moon's Position at {} is {}", current, moonPosition);

        moonRenderable.setPosition(moonPosition);
        sublunarPlacemark.setPosition(moonPosition);
    }
}
