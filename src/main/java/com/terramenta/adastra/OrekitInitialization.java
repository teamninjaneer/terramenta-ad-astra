/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.terramenta.adastra;

import org.openide.modules.OnStart;
import org.orekit.data.DataProvidersManager;

/**
 *
 * @author 510105
 */
@OnStart
public class OrekitInitialization implements Runnable {

    @Override
    public void run() {
        System.setProperty(DataProvidersManager.OREKIT_DATA_PATH, "D:/orekit-data");
    }

}
