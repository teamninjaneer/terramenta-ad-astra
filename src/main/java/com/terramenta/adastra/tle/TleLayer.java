/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.terramenta.adastra.tle;

import com.terramenta.adastra.tle.TleOrbit.OrbitPosition;
import com.terramenta.globe.utilities.RenderUtilities;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.layers.RenderableLayer;
import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.render.PreRenderable;
import gov.nasa.worldwind.render.Renderable;
import java.awt.Color;
import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.Date;
import org.hipparchus.geometry.euclidean.threed.Vector3D;
import org.orekit.bodies.BodyShape;
import org.orekit.bodies.GeodeticPoint;
import org.orekit.bodies.OneAxisEllipsoid;
import org.orekit.errors.OrekitException;
import org.orekit.frames.Frame;
import org.orekit.frames.FramesFactory;
import org.orekit.propagation.BoundedPropagator;
import org.orekit.propagation.Propagator;
import org.orekit.propagation.SpacecraftState;
import org.orekit.propagation.analytical.tle.TLE;
import org.orekit.propagation.analytical.tle.TLEPropagator;
import org.orekit.propagation.sampling.OrekitFixedStepHandler;
import org.orekit.time.AbsoluteDate;
import org.orekit.time.TimeScale;
import org.orekit.time.TimeScalesFactory;
import org.orekit.time.UTCScale;
import org.orekit.utils.Constants;
import org.orekit.utils.IERSConventions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Chris Heidt <chris.heidt@vencore.com>
 */
public class TleLayer extends RenderableLayer implements PreRenderable, Renderable {

    public static final Duration PROPAGATION_STEP_RATE = Duration.ofMinutes(1); //time between orbital position calculations
    public static final Duration BACKCAST_DURATION = Duration.ofHours(9); //length of time in which to calculate past positions
    public static final Duration FORECAST_DURATION = Duration.ofHours(3); //length of time in which to calculate future positions

    private static final Logger LOGGER = LoggerFactory.getLogger(TleLayer.class);
    private static final Frame earthFrame;
    private static final BodyShape earthBodyShape;
    private static TimeScale utcTimeScale;

    static {
        //no need to redefine frame, shape, and time scale for every layer
        Frame eFrame = null;
        BodyShape eBodyShape = null;
        UTCScale utc = null;
        try {
            eFrame = FramesFactory.getITRF(IERSConventions.IERS_2010, true);

            eBodyShape = new OneAxisEllipsoid(
                    Constants.WGS84_EARTH_EQUATORIAL_RADIUS,
                    Constants.WGS84_EARTH_FLATTENING,
                    eFrame);

            utc = TimeScalesFactory.getUTC();

        } catch (OrekitException ex) {
            throw new RuntimeException(ex);
        }

        earthFrame = eFrame;
        earthBodyShape = eBodyShape;
        utcTimeScale = utc;
    }

    private final TLEPropagator propagator;
    private final RenderUtilities.DisplayDatetimeChecker ddc = new RenderUtilities.DisplayDatetimeChecker() {

        @Override
        public void onDisplayDatetimeChange(Instant oldDatetime, Instant newDatetime) {
            propagate(newDatetime);
        }
    };

    private final TlePlacemark placemark = new TlePlacemark();
    private final TleOrbit backcastOrbit = new TleOrbit("Orbit Trail", (int) (BACKCAST_DURATION.getSeconds() / PROPAGATION_STEP_RATE.getSeconds()) + 1);
    private final TleOrbit forecastOrbit = new TleOrbit("Orbit Forecast ", (int) (FORECAST_DURATION.getSeconds() / PROPAGATION_STEP_RATE.getSeconds()) + 1);
    private BoundedPropagator ephemeris;

    /**
     *
     * @param position
     */
    public TleLayer(String name, String line1, String line2) {
        super.setValue(AVKey.DISPLAY_NAME, name);
        super.setValue(AVKey.DISPLAY_ICON, "com/terramenta/adastra/tle/satellite.png");
        super.setEnabled(true);

        forecastOrbit.setColor(Color.ORANGE);
        forecastOrbit.setStipple((short) 0x00FF);

        //bubble up enabled/disabled events to force a wwj redraw
        placemark.addPropertyChangeListener((pce) -> {
            super.firePropertyChange(pce);
        });
        backcastOrbit.addPropertyChangeListener((pce) -> {
            super.firePropertyChange(pce);
        });
        forecastOrbit.addPropertyChangeListener((pce) -> {
            super.firePropertyChange(pce);
        });

        //prevent external modification of this layers renderables
        super.setRenderables(Arrays.asList(placemark, backcastOrbit, forecastOrbit));

        //setup propagator
        TLEPropagator tleProp = null;
        try {
            TLE tle = new TLE(line1, line2);
            tleProp = TLEPropagator.selectExtrapolator(tle);
        } catch (OrekitException ex) {
            LOGGER.warn("Failed to create TLEPropagator!", ex);
            super.setEnabled(false);//disable if unable to propagate
        }

        this.propagator = tleProp;
    }

    public void propagate(Instant datetime) {
        if (datetime == null || propagator == null) {
            return;
        }

        LOGGER.info("Propagating orbital {} to {}.", this.getName(), datetime);

        //convert our Instant into an orekit AbsoluteDate
        AbsoluteDate absoluteDate = new AbsoluteDate(Date.from(datetime), utcTimeScale);

        try {
            //propagateUsingSlaveMode(absoluteDate);
            propagateUsingMasterMode(absoluteDate);
            //propagateUsingEphemerisMode(absoluteDate);
        } catch (OrekitException ex) {
            LOGGER.warn("Failed to propagate for orbital {}", this.getValue(AVKey.DISPLAY_NAME));
            return;
        }
    }

    public void propagateUsingSlaveMode(AbsoluteDate absoluteDate) throws OrekitException {
        propagator.setSlaveMode();
        SpacecraftState state = this.propagator.propagate(absoluteDate);
        Vector3D teme = state.getPVCoordinates().getPosition();
        Vector3D ecef = state.getPVCoordinates(earthFrame).getPosition();
        GeodeticPoint lla = earthBodyShape.transform(ecef, earthFrame, absoluteDate);

        backcastOrbit.addOrbitPosition(new OrbitPosition(teme, ecef));
        placemark.setPosition(Position.fromRadians(lla.getLatitude(), lla.getLongitude(), lla.getAltitude()));
    }

    public void propagateUsingMasterMode(AbsoluteDate absoluteDate) throws OrekitException {
        if (propagator.getMode() != Propagator.MASTER_MODE) {
            //we only do this once for master mode sessions
            LOGGER.info("INITILIZING MASTER MODE");
            //propagate the spacecraft to the desired datetime

            //move to starting time
            AbsoluteDate start = absoluteDate.shiftedBy(-BACKCAST_DURATION.getSeconds());
            this.propagator.propagate(start);

            propagator.setMasterMode(PROPAGATION_STEP_RATE.getSeconds(), new OrekitFixedStepHandler() {
                private int step = 0;

                @Override
                public void handleStep(SpacecraftState state, boolean last) throws OrekitException {
                    LOGGER.info("Orbital:{}; Step:{};", getName(), step);
                    step++;

                    Vector3D teme = state.getPVCoordinates().getPosition();
                    Vector3D ecef = state.getPVCoordinates(earthFrame).getPosition();
                    backcastOrbit.addOrbitPosition(new OrbitPosition(teme, ecef));

                    if (last) {
                        step = 0;
                        GeodeticPoint lla = earthBodyShape.transform(ecef, earthFrame, state.getDate());
                        placemark.setPosition(Position.fromRadians(lla.getLatitude(), lla.getLongitude(), lla.getAltitude()));
                    }
                }
            });
        }

        this.propagator.propagate(absoluteDate);
    }

    public void propagateUsingEphemerisMode(AbsoluteDate absoluteDate) throws OrekitException {
        if (ephemeris == null
                || ephemeris.getMinDate().compareTo(absoluteDate) > 0
                || ephemeris.getMaxDate().compareTo(absoluteDate) < 0) {
            //load +/- 12 hours of propogated values
            AbsoluteDate start = absoluteDate.shiftedBy(-BACKCAST_DURATION.getSeconds());
            AbsoluteDate finish = absoluteDate.shiftedBy(FORECAST_DURATION.getSeconds());
            try {
                propagator.setEphemerisMode();
                SpacecraftState finalState = propagator.propagate(start, finish);//NOTE: not using final state, keeping for now as reminder
                ephemeris = propagator.getGeneratedEphemeris();

            } catch (OrekitException ex) {
                LOGGER.warn("Failed to propagate ephemeris for orbital {} from {} till {}.",
                        this.getName(), start, finish);
                return;
            }

            LOGGER.debug("Loaded ephemeris propagations for orbital {} from {} till {}.",
                    this.getName(), ephemeris.getMinDate(), ephemeris.getMaxDate());

            //forecastOrbit.addOrbitPosition(new OrbitPosition(teme, ecef));
        }

        //propagate the spacecraft to the desired datetime
        Vector3D teme = ephemeris.propagate(absoluteDate)
                .getPVCoordinates()
                .getPosition();

        //use BodyShape to convert into a geodetic point(lla)
        GeodeticPoint lla = earthBodyShape.transform(teme, ephemeris.getFrame(), absoluteDate);

        placemark.setPosition(Position.fromRadians(lla.getLatitude(), lla.getLongitude(), lla.getAltitude()));
    }

    /**
     *
     * @param dc
     */
    @Override
    public void preRender(DrawContext dc) {
        if (dc == null) {
            return;
        }

        if (this.isEnabled()) {
            ddc.doCheck(dc);
        }
    }
}
