/**
 * Released under the DoD Community Source Usage Agreement Version 1.1 by Air
 * Force Technical Applications Center; you may not use this file except in
 * compliance with the terms and conditions of DFARS 227.7103-7 (Use and
 * non-disclosure agreement), 252.204-7000 (Disclosure of Information) and
 * 252.227-7025 (Limitations on the Use or Disclosure of Government-Furnished
 * Information Marked with Restrictive Legends). Other requests must be referred
 * to Air Force Technical Applications Center. This Software is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND. Distribution
 * authorized to U.S. Government Agencies and their contractors. Other requests
 * for this document shall be referred to Air Force Technical Applications
 * Center. Disseminate in accordance with provisions of DoD Directive 5230.25.
 * See the DCS Usage Agreement for the specific language governing permissions
 * and limitations. You may obtain a copy of the Agreement at
 * https://software.forge.mil/sf/go/doc1216
 *
 * Contract Number: FA7022-11-D-0004
 */
package com.terramenta.adastra.tle;

import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.avlist.AVListImpl;
import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.render.Renderable;
import java.awt.Color;
import java.beans.PropertyChangeEvent;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import static javax.media.opengl.GL2.GL_LINE_STIPPLE;
import org.hipparchus.geometry.euclidean.threed.Vector3D;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Chris Heidt <heidtmare@gmail.com>
 */
public class TleOrbit extends AVListImpl implements Renderable {

    private static final Logger LOGGER = LoggerFactory.getLogger(TleOrbit.class);

    public static class OrbitPosition {

        private Vector3D teme;
        private Vector3D ecef;

        public OrbitPosition(Vector3D teme, Vector3D ecef) {
            this.teme = teme;
            this.ecef = ecef;
        }

        public Vector3D getTeme() {
            return teme;
        }

        public Vector3D getEcef() {
            return ecef;
        }
    }

    public class LimitedQueue<E> extends LinkedList<E> {

        private int limit;

        public LimitedQueue(int limit) {
            this.limit = limit;
        }

        @Override
        public boolean add(E o) {
            super.add(o);
            while (size() > limit) {
                super.remove();
            }
            return true;
        }

        @Override
        public boolean addAll(Collection<? extends E> c) {
            super.addAll(c);
            while (size() > limit) {
                super.remove();
            }
            return true;
        }
    }

    private final List<OrbitPosition> positions;
    private boolean visible = true;
    private Color color = Color.YELLOW;
    private short stipple;

    /**
     *
     * @param cache
     */
    public TleOrbit(String name, int limit) {
        super.setValue(AVKey.DISPLAY_NAME, name);
        //super.setValue(AVKey.DISPLAY_ICON, "OrbitTrack.png");

        positions = new LimitedQueue<>(limit);
    }

    public List<OrbitPosition> getPositions() {
        return positions;
    }

    public void addOrbitPosition(OrbitPosition position) {
        Optional.ofNullable(position)
                .filter(op -> op.getTeme() != null)
                .filter(op -> op.getEcef() != null)
                .ifPresent(positions::add);
    }

    public void addOrbitPosition(OrbitPosition... positions) {
        List<OrbitPosition> validPositions = Arrays.stream(positions)
                .filter(Objects::nonNull)
                .filter(op -> op.getTeme() != null)
                .filter(op -> op.getEcef() != null)
                .collect(Collectors.toList());

        this.positions.addAll(validPositions);
    }

    /**
     *
     * @return
     */
    public Color getColor() {
        return color;
    }

    /**
     *
     * @param color
     */
    public void setColor(Color color) {
        this.color = color;
    }

    /**
     *
     * @return
     */
    public short getStipple() {
        return stipple;
    }

    /**
     *
     * @param color
     */
    public void setStipple(short stipple) {
        this.stipple = stipple;
    }

    /**
     *
     * @return
     */
    public boolean isVisible() {
        return visible;
    }

    /**
     *
     * @param visible
     */
    public void setVisible(boolean visible) {
        boolean old = this.visible;
        this.visible = visible;

        //this bubbles up through the parent layers and causes a redraw
        firePropertyChange(new PropertyChangeEvent(this, "Enabled", old, visible));
    }

    /**
     *
     * @param dc
     */
    @Override
    public void render(DrawContext dc) {
        if (!isVisible()) {
            return;
        }

        if (dc == null) {
            throw new IllegalArgumentException("DrawContext is null!");
        }

        GL2 gl = dc.getGL().getGL2();

        //gl.glEnable(GL.GL_TEXTURE_2D); // removed so the sun shading wouldn't effect line colors
        gl.glPushAttrib(GL2.GL_TEXTURE_BIT | GL2.GL_ENABLE_BIT | GL2.GL_CURRENT_BIT);
        gl.glMatrixMode(GL2.GL_MODELVIEW);

        // Added so that the colors wouldn't depend on sun shading
        gl.glDisable(GL.GL_TEXTURE_2D);

        // set color
        gl.glColor3f(getColor().getRed() / 255.0f, getColor().getGreen() / 255.0f, getColor().getBlue() / 255.0f); // COLOR

        if (stipple != 0) {
            gl.glLineStipple(1, stipple);
            gl.glEnable(GL_LINE_STIPPLE);
        }

        if (Boolean.TRUE.equals(dc.getValue("ECI"))) {
            drawLineECI(dc, (double) dc.getValue("ECI_ROTATION"));
        } else {
            drawLineECEF(dc);
        }

        if (stipple != 0) {
            gl.glDisable(GL_LINE_STIPPLE);
        }

        gl.glPopAttrib();
    }

    private void drawLineECEF(DrawContext dc) {
        GL2 gl = dc.getGL().getGL2();
        gl.glBegin(GL.GL_LINE_STRIP); //GL_LINE_STRIP
        positions.stream()
                .map(OrbitPosition::getEcef)
                //y,z,x
                .forEach(ecef -> gl.glVertex3f((float) ecef.getY(), (float) ecef.getZ(), (float) ecef.getX()));
        gl.glEnd();
    }

    private void drawLineECI(DrawContext dc, double degreeOfRotation) {
        GL2 gl = dc.getGL().getGL2();
        gl.glPushMatrix();
        gl.glRotated(-degreeOfRotation, 0.0, 1.0, 0.0); // rotate about Earth's spin axis (z-coordinate in J2K, y-coordinate in JOGL)
        gl.glBegin(GL.GL_LINE_STRIP); //GL_LINE_STRIP
        positions.stream()
                .map(OrbitPosition::getTeme)
                //-x,z,y
                .forEach(teme -> gl.glVertex3f((float) -teme.getX(), (float) teme.getZ(), (float) teme.getY()));
        gl.glEnd();
        gl.glPopMatrix();
    }
}
