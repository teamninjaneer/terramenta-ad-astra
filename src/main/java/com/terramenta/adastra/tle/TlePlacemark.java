/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.terramenta.adastra.tle;

import gov.nasa.worldwind.WorldWind;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.render.Material;
import gov.nasa.worldwind.render.Offset;
import gov.nasa.worldwind.render.PointPlacemark;
import gov.nasa.worldwind.render.PointPlacemarkAttributes;
import java.awt.Color;
import java.beans.PropertyChangeEvent;

/**
 *
 * @author Chris Heidt <chris.heidt@vencore.com>
 */
public class TlePlacemark extends PointPlacemark {

    /**
     *
     * @param position
     */
    public TlePlacemark() {
        super(Position.ZERO);
        super.setValue(AVKey.DISPLAY_NAME, "TLE Placemark");
        super.setValue(AVKey.DISPLAY_ICON, "com/terramenta/adastra/tle/satellite.png");
        super.setClipToHorizon(false);//they're in the air! no horizon culling.
        super.setAltitudeMode(WorldWind.ABSOLUTE);
        super.setVisible(true);
        super.setLineEnabled(true);// nadir line

        //set attributes
        PointPlacemarkAttributes attributes = new PointPlacemarkAttributes();
        attributes.setImageOffset(new Offset(0.5, 0.5, AVKey.FRACTION, AVKey.FRACTION));
        attributes.setImageAddress(TlePlacemark.class.getResource("/com/terramenta/adastra/tle/satellite32.png").toExternalForm());
        attributes.setImageColor(Color.GRAY);
        attributes.setLineMaterial(new Material(Color.GRAY));
        super.setAttributes(attributes);
    }

    public String getName() {
        return (String) super.getValue(AVKey.DISPLAY_NAME);
    }

    public void setName(String name) {
        super.setValue(AVKey.DISPLAY_NAME, name);
    }

    /**
     *
     * @param visible
     */
    @Override
    public void setVisible(boolean visible) {
        boolean old = isVisible();
        super.setVisible(visible);
        //this bubbles up through the parent layers and causes a redraw
        firePropertyChange(new PropertyChangeEvent(this, "Enabled", old, visible));
    }

}
