/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.terramenta.adastra.tle;

import com.terramenta.globe.WorldWindManager;
import com.terramenta.layermanager.creators.LayerCreator;
import gov.nasa.worldwind.layers.RenderableLayer;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.FileChooser;
import javafx.stage.Window;
import org.openide.util.Lookup;
import org.openide.util.lookup.ServiceProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author 510105
 */
@ServiceProvider(service = LayerCreator.class)
public class TleLayerCreator implements LayerCreator {

    private static final Logger LOGGER = LoggerFactory.getLogger(TleLayerCreator.class);

    @Override
    public String getName() {
        return "TLE";
    }

    @Override
    public Node getSkin() {
        Button browseButton = new Button("Select a TLE file.");
        browseButton.setOnMouseClicked(me -> {
            Window window = browseButton.getScene().getWindow();

            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Select a File");
            fileChooser.getExtensionFilters().setAll(
                    new FileChooser.ExtensionFilter("TLE Files", "*.tle")
            );

            File file = fileChooser.showOpenDialog(window);
            if (file != null) {
                RenderableLayer layer = new RenderableLayer();
                layer.addRenderables(parseTles(file.toPath()));
                layer.setName("Orbitals");
                Lookup.getDefault().lookup(WorldWindManager.class).getLayers().add(layer);

                window.hide();
            }
        });
        return new StackPane(browseButton);
    }

    private static List<TleLayer> parseTles(Path path) {
        List<TleLayer> placemarks = new ArrayList<>();

        //try with tle file resource
        try (BufferedReader buffer = Files.newBufferedReader(path)) {
            Iterator<String> it = buffer.lines().iterator();
            while (it.hasNext()) {
                TleLayer placemark = createTleLayer(it);
                if (placemark != null) {
                    placemarks.add(placemark);
                }
            }
        } catch (IOException ex) {
            LOGGER.warn("Failed to load TLE File!", ex);
        }

        return placemarks;
    }

    //example TLE
    //
    //ISS (ZARYA) //OPTIONAL LINE
    //1 25544U 98067A   08264.51782528 -.00002182  00000-0 -11606-4 0  2927
    //2 25544  51.6416 247.4627 0006703 130.5360 325.0288 15.72125391563537
    private static TleLayer createTleLayer(Iterator<String> it) {

        String name = "Unknown";
        String line1 = it.next();
        String line2 = it.next();
        if (line1 == null || line2 == null) {
            return null;
        }

        //is it a three line element set?
        if (!line1.startsWith("1 ")) {
            //line shift
            name = line1;
            line1 = line2;
            line2 = it.next();
        }

        TleLayer layer = new TleLayer(name.trim(), line1.trim(), line2.trim());
        LOGGER.info("Created TLE Layer for orbital {}", layer.getName());
        return layer;
    }

}
