/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.terramenta.adastra.actors;

import javafx.scene.image.Image;
import javafx.scene.paint.PhongMaterial;
import org.orekit.bodies.CelestialBodyFactory;
import org.orekit.errors.OrekitException;

/**
 *
 * @author Chris Heidt <chris.heidt@vencore.com>
 */
public class EarthActor extends CelestialBodyActor {

    public static final int EARTH_RADIUS_KM = 6371;
    private static final String DIFFUSE_MAP = "earth_gebco8_texture_8192x4096.jpg";
    private static final String NORMAL_MAP = "earth_normalmap_flat_8192x4096.jpg";
    private static final String SPECULAR_MAP = "earth_specularmap_flat_8192x4096.jpg";
    private static final double MAP_WIDTH = 8192 / 2d;
    private static final double MAP_HEIGHT = 4092 / 2d;
    private static final PhongMaterial earthMaterial = new PhongMaterial();

    static {
        earthMaterial.setDiffuseMap(new Image(
                EarthActor.class.getResourceAsStream(DIFFUSE_MAP),
                MAP_WIDTH,
                MAP_HEIGHT,
                true,
                true
        ));
        earthMaterial.setBumpMap(new Image(
                EarthActor.class.getResourceAsStream(NORMAL_MAP),
                MAP_WIDTH,
                MAP_HEIGHT,
                true,
                true
        ));
        earthMaterial.setSpecularMap(new Image(
                EarthActor.class.getResourceAsStream(SPECULAR_MAP),
                MAP_WIDTH,
                MAP_HEIGHT,
                true,
                true
        ));
    }

    public EarthActor(int radius) throws OrekitException {
        super(CelestialBodyFactory.getEarth(), earthMaterial, radius);
    }
}
