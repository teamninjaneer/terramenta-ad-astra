/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.terramenta.adastra.actors;

import java.time.Instant;
import javafx.beans.property.BooleanProperty;
import javafx.scene.Node;
import org.hipparchus.geometry.euclidean.threed.Vector3D;
import org.orekit.frames.Frame;

/**
 *
 * @author Chris Heidt <chris.heidt@vencore.com>
 */
public interface Actor {

    void update(Frame frame, Instant instant);

    Node getGraphic();

    Vector3D getPosition();

    public Frame getFrame();

    public BooleanProperty focusedProperty();
}
