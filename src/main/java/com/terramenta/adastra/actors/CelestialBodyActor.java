/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.terramenta.adastra.actors;

import java.time.Instant;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.paint.Material;
import javafx.scene.shape.Sphere;
import org.hipparchus.geometry.euclidean.threed.Vector3D;
import org.orekit.bodies.CelestialBody;
import org.orekit.errors.OrekitException;
import org.orekit.frames.Frame;
import org.orekit.time.AbsoluteDate;
import org.orekit.time.TimeScalesFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Chris Heidt <chris.heidt@vencore.com>
 */
public class CelestialBodyActor implements Actor {

    private static final Logger LOGGER = LoggerFactory.getLogger(CelestialBodyActor.class);
    protected final CelestialBody celestialBody;
    protected final Material material;
    protected final double radius;
    protected Sphere graphic;
    protected Vector3D position;
    protected BooleanProperty focusedProperty = new SimpleBooleanProperty(false);

    public CelestialBodyActor(CelestialBody celestialBody, Material material, double radius) {
        this.celestialBody = celestialBody;
        this.material = material;
        this.radius = radius;

        focusedProperty.addListener((obs, ov, nv) -> {
            if (nv) {
                getGraphic().setRadius(10);
            } else {
                getGraphic().setRadius(radius);
            }
        });
    }

    @Override
    public void update(Frame frame, Instant instant) {
        Vector3D vector;
        try {
            AbsoluteDate date = new AbsoluteDate(instant.toString(), TimeScalesFactory.getUTC());
            vector = celestialBody.getPVCoordinates(date, frame).getPosition();
        } catch (OrekitException ex) {
            LOGGER.warn(null, ex);
            return;
        }

        this.position = vector;
    }

    @Override
    public Sphere getGraphic() {
        if (graphic == null) {
            Sphere sphere = new Sphere(radius);
            sphere.setMaterial(material);

            graphic = sphere;
        }
        return graphic;
    }

    @Override
    public Vector3D getPosition() {
        return position;
    }

    @Override
    public Frame getFrame() {
        try {
            return celestialBody.getInertiallyOrientedFrame();
        } catch (OrekitException ex) {
            return null;
        }
    }

    @Override
    public BooleanProperty focusedProperty() {
        return focusedProperty;
    }

}
